package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.qaagility.controller.Calculator;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        int key= new Calculator().add();
        assertEquals("Add", 9, key);
        
    }
}
