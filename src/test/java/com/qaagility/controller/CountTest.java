package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.qaagility.controller.Count;

public class CountTest {
    @Test
    public void testDivide() throws Exception {

        int key= new Count().divide(10,2);
        assertEquals("Divide", 5, key);
        
    }
    
    @Test
    public void testDivideZero() throws Exception {

        int key= new Count().divide(10,0);
        assertEquals("Divide", Integer.MAX_VALUE, key);
        
    }
}
